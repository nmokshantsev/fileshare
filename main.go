package main

import (
	"database/sql"
	"fileshare/handlers"
	"fmt"
	"log"
	"net/http"

	"github.com/gorilla/mux"
	_ "github.com/mattn/go-sqlite3"
)

var db *sql.DB

func initDB() {
	var err error
	db, err = sql.Open("sqlite3", "./files.db")
	if err != nil {
		log.Fatal(err)
	}
	createTable := `
	CREATE TABLE IF NOT EXISTS files (
		id TEXT PRIMARY KEY,
		name TEXT,
		size INTEGER,
		type TEXT,
		date_uploaded TEXT
	);`
	_, err = db.Exec(createTable)
	if err != nil {
		log.Fatal(err)
	}
}

func main() {
	initDB()
	handlers.SetDB(db) // Set the database connection for the handlers

	r := mux.NewRouter()
	r.HandleFunc("/upload", handlers.Upload).Methods("POST")
	r.HandleFunc("/download/{id}", handlers.Download).Methods("GET")
	r.HandleFunc("/delete/{id}", handlers.Delete).Methods("DELETE")
	r.HandleFunc("/info/{id}", handlers.Info).Methods("GET")

	// Serve static files from the "static" directory
	r.PathPrefix("/").Handler(http.FileServer(http.Dir("./static/")))

	http.Handle("/", r)
	fmt.Println("Server is starting")
	log.Fatal(http.ListenAndServe(":8080", nil))
}
