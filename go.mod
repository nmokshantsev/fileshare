module fileshare

go 1.20

require (
	github.com/google/uuid v1.6.0
	github.com/gorilla/mux v1.8.1
)

require github.com/mattn/go-sqlite3 v1.14.22
