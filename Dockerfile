FROM golang:1.20.14-alpine3.19 AS build

#First we compile the app
COPY . /var/source

WORKDIR /var/source

RUN apk add --no-cache gcc g++ git openssh-client

RUN go mod download

RUN go mod tidy

RUN CGO_ENABLED=1 GOOS=linux GOARCH=amd64 go build -v -o fileshare_server main.go

#stage 2

FROM alpine:3.19 

RUN adduser --home /home/web -D web

USER web

RUN mkdir /home/web/app

WORKDIR /home/web/app

COPY --from=build --chown=web:web /var/source/fileshare_server .

CMD ["./fileshare_server" ]
