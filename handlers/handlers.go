package handlers

import (
	"database/sql"
	"fmt"
	"io"
	"net/http"
	"os"
	"path/filepath"
	"time"

	"github.com/google/uuid"
	"github.com/gorilla/mux"
	_ "github.com/mattn/go-sqlite3"
)

var db *sql.DB

func SetDB(database *sql.DB) {
	db = database
}

func Upload(w http.ResponseWriter, r *http.Request) {
	// Limit the size of the request body to 50MB
	r.Body = http.MaxBytesReader(w, r.Body, 50<<20) // 50 MB

	// Parse the multipart form
	err := r.ParseMultipartForm(50 << 20) // 50 MB max file size
	if err != nil {
		http.Error(w, "File is too large", http.StatusRequestEntityTooLarge)
		return
	}

	// Get the file from the form
	file, handler, err := r.FormFile("file")
	if err != nil {
		http.Error(w, "Error retrieving the file", http.StatusInternalServerError)
		return
	}
	defer file.Close()

	// Generate a unique file ID
	fileID := uuid.New().String()

	// Create a directory to save the file if it doesn't exist
	uploadDir := "./uploads"
	if _, err := os.Stat(uploadDir); os.IsNotExist(err) {
		os.Mkdir(uploadDir, os.ModePerm)
	}

	// Create the file on the server
	filePath := filepath.Join(uploadDir, fileID)
	dst, err := os.Create(filePath)
	if err != nil {
		http.Error(w, "Error creating the file", http.StatusInternalServerError)
		return
	}
	defer dst.Close()

	// Copy the uploaded file to the destination
	if _, err := io.Copy(dst, file); err != nil {
		http.Error(w, "Error saving the file", http.StatusInternalServerError)
		return
	}

	// Insert file metadata into the database
	stmt, err := db.Prepare("INSERT INTO files (id, name, size, type, date_uploaded) VALUES (?, ?, ?, ?, ?)")
	if err != nil {
		http.Error(w, "Error preparing database statement", http.StatusInternalServerError)
		return
	}
	defer stmt.Close()

	_, err = stmt.Exec(fileID, handler.Filename, handler.Size, handler.Header.Get("Content-Type"), time.Now().Format(time.RFC3339))
	if err != nil {
		http.Error(w, "Error inserting file metadata into the database", http.StatusInternalServerError)
		return
	}

	// Respond with the file ID
	w.WriteHeader(http.StatusOK)
	fmt.Fprintf(w, "%s", fileID)
}

func Download(w http.ResponseWriter, r *http.Request) {
	vars := mux.Vars(r)
	fileID := vars["id"]

	var filename string
	err := db.QueryRow("SELECT name FROM files WHERE id = ?", fileID).Scan(&filename)
	if err != nil {
		http.Error(w, "File not found", http.StatusNotFound)
		return
	}

	filePath := filepath.Join("./uploads", fileID)
	file, err := os.Open(filePath)
	if err != nil {
		http.Error(w, "Error opening the file", http.StatusInternalServerError)
		return
	}
	defer file.Close()

	w.Header().Set("Content-Disposition", fmt.Sprintf("attachment; filename=%s", filename))
	w.Header().Set("Content-Type", "application/octet-stream")
	http.ServeFile(w, r, filePath)
}
func Delete(w http.ResponseWriter, r *http.Request) {
	vars := mux.Vars(r)
	fileID := vars["id"]

	_, err := db.Exec("DELETE FROM files WHERE id = ?", fileID)
	if err != nil {
		http.Error(w, "Error deleting file info", http.StatusInternalServerError)
		return
	}

	filePath := filepath.Join("./uploads", fileID)
	err = os.Remove(filePath)
	if err != nil {
		http.Error(w, "Error deleting file", http.StatusInternalServerError)
		return
	}

	w.WriteHeader(http.StatusOK)
	fmt.Fprintf(w, "File deleted successfully!")
}

func Info(w http.ResponseWriter, r *http.Request) {
	vars := mux.Vars(r)
	fileID := vars["id"]

	var name string
	var size int
	var fileType string
	var dateUploaded string
	err := db.QueryRow("SELECT name, size, type, date_uploaded FROM files WHERE id = ?", fileID).Scan(&name, &size, &fileType, &dateUploaded)
	if err != nil {
		http.Error(w, "File not found", http.StatusNotFound)
		return
	}

	fileInfo := fmt.Sprintf("Name: %s\nSize: %d\nType: %s\nDate Uploaded: %s", name, size, fileType, dateUploaded)
	w.WriteHeader(http.StatusOK)
	w.Write([]byte(fileInfo))
}
